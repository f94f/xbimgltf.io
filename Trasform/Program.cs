﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xbim.Common.Geometry;
using Xbim.GLTF;
using Xbim.GLTF.SemanticExport;
using Xbim.Ifc;
using Xbim.ModelGeometry.Scene;

namespace Trasform
{
    class Program
    {
        private static string PATH_SCARICA_IFC = "Files/input";
        private static string PATH_OUT_TRASFORM = "Files/output";
        public static string msg = "";

        static void Main(string[] args)
        {
            run(args);
        }

        public static String run(string[] args)
        {
            args = new string[3];
            args[0] = "path";
            args[1] = "Files/input/wall.ifc";
            args[2] = null;
            //args[0] = "url";
            //args[1] = "http://127.0.0.1:8000/polls/file";
            //args[2] = null;

            /**
             * 0 - Codice per sapere se è una url o un path
             * 1 - URL/path del file a trasformare
             * 2 - path dove lasciare i file trasformati (glTF/json) [OPCIONALE]
             * 3 - Nome del file [OPCIONALE]
             */
            string resp = "Tutto perfetto";

            //Controllo ci siano tutti gli argomenti
            if (args.Length > 1 && args.Length < 5)
            {
                string code = args[0];
                string url_path = args[1];
                string out_path = args.Length > 2 ? args[2] : null;
                string nome = args.Length > 3 ? args[3] : null;
                if (code.ToLower().Equals("url"))
                {
                    string fileName = "";
                    //Provo a scaricare il file
                    if (scaricaFile(url_path, ref fileName))
                    {
                        //Il file è stato scaricato
                        trasformareFile(fileName, out_path);
                    }
                    else
                    {
                        resp = "Non è statu possibile scaricare il file";
                    }
                }
                else if (code.ToLower().Equals("path"))
                {
                    //Cerco il file
                    if (File.Exists(url_path))
                    {
                        trasformareFile(url_path, out_path);
                    }
                    else
                    {
                        resp = "Il file non è statu trovato";
                    }
                }
                else
                {
                    //Il codice di ricerca non è corretto
                    resp = "Parametro non riconosciuto";
                }
            }
            else
            {
                //Non ci sono argomenti o ci sono di più
                resp = "Gli argomenti sono sbagliati, ci sono di più o di meno";
            }
            return resp;
        }

        //Scarica il file IFC
        public static bool scaricaFile(string url, ref string fileName)
        {
            bool resp = false;
            try
            {
                using (var client = new WebClient())
                {
                    //client.DownloadFile(url, PATH_SCARICA_IFC + "");
                    var data = client.DownloadData(url);
                    fileName = PATH_SCARICA_IFC + "/" + Path.GetFileName(url) + ".ifc";
                    File.WriteAllBytes(fileName, data);

                    //if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
                    //{
                    //    fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
                    //}
                }
                resp = true;
            }
            catch (Exception e)
            {
                //Exception log
                msg = e.Message;
            }
            return resp;
        }

        public static bool trasformareFile(string filePath, string out_path)
        {
            bool resp = false;
            var ifc = new FileInfo(filePath);
            var xbim = CreateGeometry(ifc, true, false);
            out_path = out_path == null ? PATH_OUT_TRASFORM : out_path;

            if (xbim != null)
            {
                using (var s = IfcStore.Open(xbim.FullName))
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    string[] split_outS = s.FileName.Split('\\');
                    string fileName = split_outS[split_outS.Length - 1];
                    var savename = Path.ChangeExtension(fileName, ".gltf");
                    out_path = Path.Combine(out_path, savename);

                    // creando il file glTF
                    //
                    try
                    {
                        var bldr = new Builder();
                        var ret = bldr.BuildInstancedScene(s, XbimMatrix3D.Identity);
                        glTFLoader.Interface.SaveModel(ret, out_path);

                        FileInfo f = new FileInfo(s.FileName);

                        // write json
                        //
                        try
                        {
                            var jsonFileName = Path.ChangeExtension(out_path, "json");
                            var bme = new BuildingModelExtractor();
                            var rep = bme.GetModel(s);
                            rep.Export(jsonFileName);
                            resp = true;
                        }
                        catch (Exception e)
                        {
                            //Errore nel JSON
                            msg = e.Message;
                        }
                    }
                    catch (Exception e)
                    {
                        //Errore nel glTF
                        msg = e.Message;
                    }
                    sw.Stop();
                }
            }

            return resp;
        }

        private static FileInfo CreateGeometry(FileInfo f, bool mode, bool useAlternativeExtruder)
        {
            try
            {
                //IfcStore.ModelProviderFactory.UseMemoryModelProvider(); //MemoryModel
                IfcStore.ModelProviderFactory.UseHeuristicModelProvider(); //Esent
                using (var m = IfcStore.Open(f.FullName))
                {
                    var c = new Xbim3DModelContext(m);
                    c.CreateContext(null, mode);
                    var newName = Path.ChangeExtension(f.FullName, mode + ".xbim");
                    m.SaveAs(newName);
                    return new FileInfo(newName);
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
                return null;
            }

        }

        #region Prove
        public static void prueba_exe(string[] args)
        {
            //string fileName = @"C:\Temp\Mahesh.txt";
            string fileName = @"Files\Mahesh.txt";

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (FileStream fs = File.Create(fileName))
                {
                    // Add some text to file    
                    byte[] texto = new UTF8Encoding(true).GetBytes("New Text File");
                    fs.Write(texto, 0, texto.Length);
                    texto = new UTF8Encoding(true).GetBytes("Mahesh Chand \n");
                    fs.Write(texto, 0, texto.Length);
                    texto = new UTF8Encoding(true).GetBytes("Arguments \n");
                    fs.Write(texto, 0, texto.Length);

                    foreach (var a in args)
                    {
                        byte[] b = new UTF8Encoding(true).GetBytes(a + " \n");
                        fs.Write(b, 0, b.Length);
                    }
                }

                // Open the stream and read it back.    
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        public static void prova_desc(String aaa)
        {
            string fileName = "";
            string url = "https://upload.wikimedia.org/wikipedia/en/a/a8/HELLOWORLD.jpg";
            using (var client = new WebClient())
            {
                //client.DownloadFile(url, @"D:\temp\imagen.jpg");

                var data = client.DownloadData(url);

                fileName = Path.GetFileName(url);

                // Try to extract the filename from the Content-Disposition header
                if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
                {
                    fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
                }

                //fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");

                File.WriteAllBytes(@"D:\temp\imagen.jpg", data);
            }
        }

        public static async System.Threading.Tasks.Task DownloadFileAsync(String url)
        {
            string bookPath_Pdf = @"D:\VisualStudio\randomfile.pdf";
            string bookPath_xls = @"D:\VisualStudio\randomfile.xls";
            string bookPath_doc = @"D:\VisualStudio\randomfile.docx";
            string bookPath_zip = @"D:\VisualStudio\randomfile.zip";

            string format = "pdf";
            string reqBook = format.ToLower() == "pdf" ? bookPath_Pdf : (format.ToLower() == "xls" ? bookPath_xls : (format.ToLower() == "doc" ? bookPath_doc : bookPath_zip));
            string fileName = "sample." + format.ToLower();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:49209/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("applicaiton/json"));

                    Console.WriteLine("GET");

                    //converting Pdf file into bytes array
                    var dataBytes = File.ReadAllBytes(reqBook);

                    //adding bytes to memory stream
                    var dataStream = new MemoryStream(dataBytes);

                    //send request asynchronously
                    HttpResponseMessage response = await client.GetAsync("api/person");
                    response.Content = new StreamContent(dataStream);
                    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = fileName;
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                    //Check that response was successful or throw exception
                    //response.EnsureSuccessStatusCode();

                    //Read response asynchronously and save asynchronously to file
                    if (response.IsSuccessStatusCode)
                    {
                        using (var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:49209/api"))
                        {
                            using (
                            Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                                fileStream = new FileStream("D:\\VisualStudio\\randomfile.pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                //copy the content from response to filestream
                                await response.Content.CopyToAsync(fileStream);
                                //Console.WriteLine();
                            }

                        }
                    }

                }
            }
            catch (HttpRequestException rex)
            {
                Console.WriteLine(rex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void CanConvertOneFile(String finalPathString)
        {
            var ifc = new FileInfo(finalPathString);
            var xbim = CreateGeometry(ifc, true, false);

            using (var s = IfcStore.Open(xbim.FullName))
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                //Console.WriteLine("s.FileName: " + s.FileName);
                String outS = s.FileName.Replace("input", "output");
                string[] split_outS = outS.Split('\\');
                //Console.WriteLine("outS: " + outS);

                var savename = Path.ChangeExtension(outS, ".gltf");
                var bldr = new Builder();
                var ret = bldr.BuildInstancedScene(s, XbimMatrix3D.Identity);
                glTFLoader.Interface.SaveModel(ret, savename);

                //Debug.WriteLine($"Gltf Model exported to '{savename}' in {sw.ElapsedMilliseconds} ms.");
                FileInfo f = new FileInfo(s.FileName);

                // write json
                //
                var jsonFileName = Path.ChangeExtension(outS, "json");
                var bme = new BuildingModelExtractor();
                var rep = bme.GetModel(s);
                rep.Export(jsonFileName);
                sw.Stop();
            }
            Console.WriteLine("I File sono stati creati correttamente");
        }

        #endregion
    }

}
